//import dependencies modules
const express = require('express');
//init express (create the server)
var app = express();
//port number
const port =3000;
app.listen(port,function(){
    console.log('Server started at port:'+port);
});

//Test server
app.get('/', (req, res)=>{res.send(`Hello World from  ${process.env.APP_NAME || "<No APP Name!>"}`);});


